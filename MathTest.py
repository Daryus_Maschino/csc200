from random import randint

print("Welcome to Daryus' super cool math quiz!")
correct = 0
total = 0

while correct <= 10
    intone = randint(1,12)
    inttwo = randint(1,12)
    print("What's",intone,"multiplied by",inttwo,"?")
    solution = int(input)
    if solution == intone * inttwo
        print("You got it right, good job!")
        correct += 1
        total += 1
    else print("You got it wrong, better luck next time")
    total += 1
if correct == 10
print("Out of",total,"you got",correct,"questions right."
