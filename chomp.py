from gasp import *

GRID_SIZE = 30
MARGIN = GRID_SIZE

BACKGROUND_COLOR = color.BLACK
WALL_COLOR = "#99E5E5"


class Immovable:
    pass


class Nothing(Immovable):
    pass


class Wall(Immovable):
    def __init__(self, maze, point):
        self.place = point
        self.screen_point = maze.to_screen(point)
        self.maze = maze
        self.draw()

    def draw(self):
        (screen_x, screen_y) = self.screen_point
        dot_size = GRID_SIZE * 0.2
        Circle(self.screen_point, dot_size, color=WALL_COLOR, filled=True)


class Maze:
    def __init__(self):
        self.have_window = False
        self.game_over = False
        self.get_level()

    def get_level(self):
        f = open('layout.dat')
        self.the_layout = []
        for line in f.readlines():
            self.the_layout.append(line.rstrip())

    def set_layout(self):
         height = len(self.the_layout)
         width = len(self.the_layout[0])
         self.make_window(height, width)
         self.make_map()

    def finished(self):
        return self.game_over

    def play(self):
        answered = input('Are we done yet? ')
        if answered == 'y':
            self.game_over = True
        else:
            print('I\'m playing')

    def done(self):
        print('I\'m done now.')
        end_graphics()
        self.map = []

    def make_window(self, width, height):
        grid_width = (width - 1) * GRID_SIZE  # Work out size of window
        grid_height = (height - 1) * GRID_SIZE
        screen_width = 2 * MARGIN + grid_width
        screen_height = 2 * MARGIN + grid_height
        begin_graphics(screen_width, screen_height, "Chomp", BACKGROUND_COLOR)

    def to_screen(self, point):
        (x, y) = point
        x = x * GRID_SIZE + MARGIN  # Work out coordinates of point
        y = y * GRID_SIZE + MARGIN  # on screen
        return (x, y)

    def make_object(self, point, character):
        (x, y) = point
        if character == "%":
            self.map[y][x] = Wall(self, point)


the_maze = Maze()
    print(the_maze.the_layout)

    while not the_maze.finished():
    the_maze.play()

    the_maze.done()

